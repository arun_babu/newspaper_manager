<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Redirect::route('login');

});

//Auth::routes();
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@loginWithOtp');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::post('/verify', 'Auth\LoginController@verifyPhone')->name('verify-phone');
Route::post('/verify-code', 'Auth\LoginController@verifyCode')->name('verify-code');


// Registration Routes...
Route::middleware(['auth'])->group(function () {
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register');
    Route::get('/home', 'UploadController@search')->name('home');
    Route::get('/profile', 'ProfileController@form');
    Route::get('/my-profile', 'ProfileController@profile');
//////////
    Route::get('/crop/{id}', 'UploadController@crop');
    Route::get('/search', 'UploadController@search');
    Route::get('/list/{id}', 'UploadController@lists');
    Route::get('/test', 'UploadController@test');
    Route::get('/testing', 'UploadController@testing');
    Route::post('/save', 'UploadController@save');
    Route::get('/testsave', 'UploadController@testsubmit');
});

//home


//Profile

//Route::middleware(['auth', 'is_admin'])->group(function () {
Route::middleware(['auth', 'is_admin'])->group(function () {

//Publication
    Route::get('/publication', 'PublicationController@index');
    Route::get('/publication/form', 'PublicationController@form');
    Route::post('/publication/store', 'PublicationController@store');
    Route::get('/publication/{id}/edit', 'PublicationController@edit');
    Route::get('/publication/{id}/delete', 'PublicationController@delete');

//upload
    Route::get('/getPublication', 'PublicationController@getPublication');

    Route::get('/upload', 'UploadController@upload');
    Route::get('/name', 'UploadController@name');
    Route::get('/isExist', 'UploadController@isExist');
    Route::get('/deleteExist', 'UploadController@deleteExist');
    Route::get('/users', 'ProfileController@index');
    Route::get('/users/{id}', 'ProfileController@edit');
    Route::post('/users/store', 'ProfileController@store');

});
