<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>JavaScript Image Cropping with DropzoneJS</title>
    <link href="https://unpkg.com/dropzone/dist/dropzone.css" rel="stylesheet"/>
    <link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet"/>
</head>
<body>
<!-- <div class="dropzone" id="myDropzone"></div> -->

<form class="dropzone" id="myDropzone">
    {{ csrf_field() }}
</form>
<button type="button" class="btn btn-info" id="submit-all">Submit All</button>

<script src="https://unpkg.com/dropzone"></script>
<script src="https://unpkg.com/cropperjs"></script>

<script>
    Dropzone.options.myDropzone = {
        autoProcessQueue: false,
        uploadMultiple: true,
        acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
        init: function () {
            var submitButton = document.querySelector('#submit-all');
            myDropzone = this;
            submitButton.addEventListener("click", function () {
                myDropzone.processQueue();
            });
            this.on("complete", function () {
                alert('complete')
                if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
                    var _this = this;
                    _this.removeAllFiles();
                }
                list_image();
            });
        },
    };

    Dropzone.options.myDropzone = {
        url: 'save',

        transformFile: function (file, done) {
            var myDropZone = this;
            // Create the image editor overlay
            var editor = document.createElement('div');
            editor.style.position = 'fixed';
            editor.style.left = 0;
            editor.style.right = 0;
            editor.style.top = 0;
            editor.style.bottom = 0;
            editor.style.zIndex = 9999;
            editor.style.backgroundColor = '#000';
            document.body.appendChild(editor);

            // Create confirm button at the top left of the viewport
            var buttonConfirm = document.createElement('button');
            buttonConfirm.style.position = 'absolute';
            buttonConfirm.style.left = '10px';
            buttonConfirm.style.top = '10px';
            buttonConfirm.style.zIndex = 9999;
            buttonConfirm.textContent = 'Confirm';
            editor.appendChild(buttonConfirm);


            buttonConfirm.addEventListener('click', function () {
                // Get the canvas with image data from Cropper.js
                var canvas = cropper.getCroppedCanvas({
                    width: 256,
                    height: 256,
                    autoCropArea: 1,
                    aspectRatio: 9 / 16,
                    cropBoxResizable: false,
                    movable: true,
                    rotatable: true,
                    scalable: true,
                    viewMode: 2,
                    minContainerWidth: 250,
                    maxContainerWidth: 250
                });
                // Turn the canvas into a Blob (file object without a name)
                canvas.toBlob(function (blob) {
                    // Return the file to Dropzone
                    // Create a new Dropzone file thumbnail
                    myDropZone.createThumbnail(
                        blob,
                        myDropZone.options.thumbnailWidth,
                        myDropZone.options.thumbnailHeight,
                        myDropZone.options.thumbnailMethod,
                        false,
                        function (dataURL) {
                            // Update the Dropzone file thumbnail
                            console.log('OOOOOO', file)
                            myDropZone.emit('thumbnail', file, dataURL);
                            // Return the file to Dropzone
                            done(blob);
                        });
                });
                // Remove the editor from the view
                document.body.removeChild(editor);
            });


            file.previewElement.addEventListener("click", function () {

                var myDropZone = this;
                // Create the image editor overlay
                var editor = document.createElement('div');
                editor.style.position = 'fixed';
                editor.style.left = 0;
                editor.style.right = 0;
                editor.style.top = 0;
                editor.style.bottom = 0;
                editor.style.zIndex = 9999;
                editor.style.backgroundColor = '#000';
                document.body.appendChild(editor);

                // Create confirm button at the top left of the viewport
                var buttonConfirm = document.createElement('button');
                buttonConfirm.style.position = 'absolute';
                buttonConfirm.style.left = '10px';
                buttonConfirm.style.top = '10px';
                buttonConfirm.style.zIndex = 9999;
                buttonConfirm.textContent = 'Confirm';
                editor.appendChild(buttonConfirm);

                buttonConfirm.addEventListener('click', function () {
                    // Get the canvas with image data from Cropper.js
                    var canvas = cropper.getCroppedCanvas({
                        width: 256,
                        height: 256,
                        autoCropArea: 1,
                        aspectRatio: 9 / 16,
                        cropBoxResizable: false,
                        movable: true,
                        rotatable: true,
                        scalable: true,
                        viewMode: 2,
                        minContainerWidth: 250,
                        maxContainerWidth: 250
                    });
                    // Turn the canvas into a Blob (file object without a name)
                    canvas.toBlob(function (blob) {
                        // Return the file to Dropzone
                        // Create a new Dropzone file thumbnail
                        myDropZone.createThumbnail(
                            blob,
                            myDropZone.options.thumbnailWidth,
                            myDropZone.options.thumbnailHeight,
                            myDropZone.options.thumbnailMethod,
                            false,
                            function (dataURL) {
                                // Update the Dropzone file thumbnail
                                console.log('OOOOOO', file)
                                myDropZone.emit('thumbnail', file, dataURL);
                                // Return the file to Dropzone
                                done(blob);
                            });
                        alert('looll')
                    });
                    // Remove the editor from the view
                    document.body.removeChild(editor);
                });



            });


// Create an image node for Cropper.js
            var image = new Image();
            image.src = URL.createObjectURL(file);
            editor.appendChild(image);
            // Create Cropper.js
            var cropper = new Cropper(image, {aspectRatio: 1});

        }
    };


</script>
</body>
</html>