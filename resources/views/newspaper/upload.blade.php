@extends('layouts.app')
@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.2.0/min/dropzone.min.css" rel="stylesheet"/>
    <link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>

    <script src="https://code.jquery.com/jquery-2.1.4.js"
            integrity="sha256-siFczlgw4jULnUICcdm9gjQPZkw/YPDqhQ9+nAOScE4="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.3/cropper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.2.0/min/dropzone.min.js"></script>
    <script src="https://unpkg.com/cropperjs.js"></script>
    {{--<script src="{{asset('js/plugins/bootstrap-datetimepicker.js')}}"></script>--}}


    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">--}}
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <!---- HTML for Dropzone -->

    <div>
        <form class="dropzone" id="my-dropzone" style="border: 2px dashed #0087F7 ; border-radius: 5px;
">
            {{ csrf_field() }}
            <input type="hidden" name="publication" id="dropzonePublication" value="">
            <input type="hidden" name="date" id="dropzoneDate" value="">
        </form>
        <button type="button" class="btn btn-info" id="submit-all">Upload</button>
    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            {{--<input id="datepicker" value=""/>--}}
                            <div class="card-body ">
                                <div class="form-group">
                                    <input type="text" class="form-control datepicker" id="datepicker"
                                           value="{{date("m/d/Y")}}" name="date">
                                </div>

                            </div>
                        </div>
                        <br/>
                        <br/>
                        <br/>
                        <div class="col-md-12">
                            <div class="card-body ">
                                <div class="form-group">
                                    <select class="browser-default custom-select " id="publication">
                                        <option value="null" selected>Select Daily</option>
                                        @foreach($publications as $each)
                                            <option value="{{$each->id}}">{{$each->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="confirm">Confirm</button>
                </div>
            </div>

        </div>
    </div>
    <script src="{{asset('/js/plugins/moment.min.js')}}"></script>

    <script>

        $(document).ready(function () {
            demo.initDateTimePicker();
            $('#datepicker').datepicker();
            Dropzone.options.myDropzone = {
                autoProcessQueue: false,
                uploadMultiple: true,
                acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
                init: function () {
                    var submitButton = document.querySelector('#submit-all');
                    var myModal = document.getElementById("myModal");
                    myDropzone = this;
                    submitButton.addEventListener("click", function () {
                        var $submitModal = $(myModal);
                        $submitModal.modal('show').on("shown.bs.modal")
                            .on('click', '#confirm', function () {
                                var datepicker = $('#datepicker').val();
                                var publication = $('#publication').val();
                                $.ajax({
                                    url: "isExist?publication=" + publication + "&datepicker=" + datepicker,
                                    success: function (result) {
                                        if (result.status) {
                                            swal({
                                                title: 'Are you sure?',
                                                text: "The folder already exists. Do you want to overwrite it?",
                                                type: 'warning',
                                                showCancelButton: true,
                                                confirmButtonClass: 'btn btn-success',
                                                cancelButtonClass: 'btn btn-danger',
                                                confirmButtonText: 'Yes, overwrite!',
                                                buttonsStyling: false
                                            }).then(function () {
                                                var datepicker = $submitModal.find('#datepicker').val();
                                                var publication = $submitModal.find('#publication').val();

                                                $.ajax({
                                                    url: "deleteExist?publication=" + publication + "&datepicker=" + datepicker,
                                                    success: function (result) {
                                                        if (result.status) {
                                                            if (publication == "null") {
                                                                alert('select any publication')
                                                            } else {
                                                                document.getElementById("dropzoneDate").setAttribute("value", datepicker);
                                                                document.getElementById("dropzonePublication").setAttribute("value", publication);
                                                                if (myDropzone.getQueuedFiles().length)
                                                                    myDropzone.processQueue();
                                                                $submitModal.modal('hide');
                                                                document.getElementById("dropzoneDate").setAttribute("value", '');
                                                                document.getElementById("dropzonePublication").setAttribute("value", '');
                                                            }
                                                        } else {
                                                            alert('Something went wrong!!!')
                                                        }
                                                    }
                                                })
                                            }).catch(swal.noop);
                                            // if (confirm('The folder already exists. Do you want to overwrite it?')) {
                                            //
                                            // }
                                        } else {
                                            var datepicker = $submitModal.find('#datepicker').val();
                                            var publication = $submitModal.find('#publication').val();
                                            if (publication == "null") {
                                                alert('select any publication')
                                            } else {
                                                document.getElementById("dropzoneDate").setAttribute("value", datepicker);
                                                document.getElementById("dropzonePublication").setAttribute("value", publication);
                                                if (myDropzone.getQueuedFiles().length)
                                                    myDropzone.processQueue();
                                                $submitModal.modal('hide');
                                                document.getElementById("dropzoneDate").setAttribute("value", '');
                                                document.getElementById("dropzonePublication").setAttribute("value", '');
                                            }
                                        }
                                    }
                                });
                            });
                    });
                    this.on("complete", function () {
                        if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
                            var _this = this;
                            _this.removeAllFiles();
                        }
                        // list_image();
                    });
                },
            };

            var Cropper = window.Cropper;
            Dropzone.autoDiscover = false;
            var c = 0;
            var cropped = false;
            var myDropzone = new Dropzone('#my-dropzone', {
                url: "save",
                addRemoveLinks: true,
                parallelUploads: 50,
                uploadMultiple: false,
                createImageThumbnails: true,
                autoProcessQueue: false,
                maxFiles: 500,
                maxFilesize: 500,
                maxThumbnailFilesize: 40,
                // parallelChunkUploads: true,
                acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
                timeout: 500000,
                success: function (file, response) {
                },
                queuecomplete: function () {
                    demo.showNotification('top', 'right');
                }
            });

            $('.button-click').on('click', function () {
                if (!cropped) {
                    myDropzone.removeFile(file);
                    cropper(file);
                } else {
                    cropped = false;
                    var previewURL = URL.createObjectURL(file);
                    var dzPreview = $(file.previewElement).find('img');
                    dzPreview.attr("src", previewURL);
                }
            });

            myDropzone.on('addedfile', function (file) {
                file.previewElement.addEventListener("click", function () {
                    // myDropzone.removeFile(file);
                    cropper(file);
                });

            });


            var cropper = function (file) {
                var fileName = file.name;
                var loadedFilePath = getSrcImageFromBlob(file);
                // @formatter:off
                var modalTemplate =
                    '<div class="modal fade" tabindex="-1" role="dialog">' +
                    '<div class="modal-dialog modal-xl" role="document">' +
                    '<div class="modal-content " >' +
                    '<div class="modal-header">' +
                    '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>' +
                    '</div>' +
                    '<div class="modal-body">' +
                    '<div class="cropper-container">' +
                    '<img id="img-' + ++c + '" src="' + loadedFilePath + '" data-vertical-flip="false" data-horizontal-flip="false">' +
                    '</div>' +
                    '</div>' +
                    '<div class="modal-footer">' +
                    '<button type="button" class="btn btn-warning rotate-left"><span class="fa fa-rotate-left"></span></button>' +
                    '<button type="button" class="btn btn-warning rotate-right"><span class="fa fa-rotate-right"></span></button>' +
                    // '<button type="button" class="btn btn-warning scale-x" data-value="-1"><span class="fa fa-arrows-h"></span></button>' +
                    // '<button type="button" class="btn btn-warning scale-y" data-value="-1"><span class="fa fa-arrows-v"></span></button>' +
                    '<button type="button" class="btn btn-warning reset"><span class="fa fa-refresh"></span></button>' +
                    '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
                    '<button type="button" class="btn btn-primary crop-upload">Crop & upload</button>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
                // @formatter:on

                var $cropperModal = $(modalTemplate);
                $cropperModal.modal('show').on("shown.bs.modal", function () {
                    var $image = $('#img-' + c);
                    console.log($image);
                    var cropper = $image.cropper({
                        autoCropArea: 1,
                        // aspectRatio: 9 / 16,
                        cropBoxResizable: true,
                        movable: true,
                        rotatable: true,
                        scalable: true,
                        viewMode: 2,
                        minContainerWidth: 250,
                        maxContainerWidth: 250,
                        zoomOnWheel: true

                    })
                        .on('hidden.bs.modal', function () {
                            $image.cropper('destroy');
                        });

                    $cropperModal.on('click', '.crop-upload', function () {
                        // get cropped image data
                        var cropp = $image.cropper('getCroppedCanvas', {
                            width: 90,
                            height: 90,
                            minWidth: 256,
                            minHeight: 256,
                            maxWidth: 4096,
                            maxHeight: 4096,
                            fillColor: '#fff',
                            imageSmoothingEnabled: false,
                            imageSmoothingQuality: 'high'
                        });
                        cropp.toBlob(function (blob) {

                            myDropzone.createThumbnail(
                                blob,
                                myDropzone.options.thumbnailWidth,
                                myDropzone.options.thumbnailHeight,
                                myDropzone.options.thumbnailMethod,
                                true,
                                function (dataURL) {
                                    var croppedFile = blobToFile(blob, fileName);
                                    croppedFile.accepted = true;
                                    // Update the Dropzone file thumbnail
                                    myDropzone.emit('thumbnail', croppedFile, dataURL);
                                    // var croppedFile = blobToFile(blob, fileName);
                                    // Return the file to Dropzone
                                    // myDropzone.emit('addedfile', file);
                                    // myDropzone.files.push(blob);
                                    // done(blob);
                                });


                            var croppedFile = blobToFile(blob, fileName);
                            croppedFile.accepted = true;
                            var files = myDropzone.getAcceptedFiles();
                            for (var i = 0; i < files.length; i++) {
                                var file = files[i];
                                if (file.name === fileName) {
                                    myDropzone.removeFile(file);
                                }
                            }
                            cropped = true;
                            myDropzone.files.push(croppedFile);
                            myDropzone.emit('addedfile', croppedFile);
                        });
                        $cropperModal.modal('hide');
                    })
                        .on('click', '.rotate-right', function () {
                            $image.cropper('rotate', 10);
                        })
                        .on('click', '.rotate-left', function () {
                            $image.cropper('rotate', -10);
                        })
                        .on('click', '.reset', function () {
                            $image.cropper('reset');
                        })
                        .on('click', '.scale-x', function () {
                            if (!$image.data('horizontal-flip')) {
                                $image.cropper('scale', -1, 1);
                                $image.data('horizontal-flip', true);
                            } else {
                                $image.cropper('scale', 1, 1);
                                $image.data('horizontal-flip', false);
                            }
                        })
                        .on('click', '.scale-y', function () {
                            if (!$image.data('vertical-flip')) {
                                $image.cropper('scale', 1, -1);
                                $image.data('vertical-flip', true);
                            } else {
                                $image.cropper('scale', 1, 1);
                                $image.data('vertical-flip', false);
                            }
                        });
                });
            };

            function getSrcImageFromBlob(blob) {
                var urlCreator = window.URL || window.webkitURL;
                return urlCreator.createObjectURL(blob);
            }

            function blobToFile(theBlob, fileName) {
                theBlob.lastModifiedDate = new Date();
                theBlob.name = fileName;
                theBlob.upload = {'filename': fileName}
                theBlob.status = "queued";
                return theBlob;
            }
        });


    </script>
@endsection
