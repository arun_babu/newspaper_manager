@extends('layouts.app')
@section('content')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="images" content="{{$images}}">
    <meta name="date" content="{{ $upload->date }}">
    <meta name="publication" content="{{ $upload->publication }}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://fengyuanchen.github.io/cropperjs/css/cropper.css">
    <link rel="stylesheet" href="https://fengyuanchen.github.io/cropperjs/css/main.css">
    <link href="{{asset('slider/css/ninja-slider.css')}}" rel="stylesheet"/>
    <script src="{{asset('slider/js/ninja-slider.js')}}"></script>
    <link href="{{asset('slider/css/thumbnail-slider.css')}}" rel="stylesheet" type="text/css"/>
    <script src="{{asset('slider/js/thumbnail-slider.js')}}" type="text/javascript"></script>

    <div class="container">
        <div class="row" id="actions">
            <div class="col-md-9 docs-buttons">
                <div class=" btn-group">
                    <a href="{{url('/list/'.$upload->id)}}">
                        <button type="button" class="btn btn-danger">
            <span class="docs-tooltip">
              Back
            </span>
                        </button>

                    </a>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-primary backward-click" data-method="setDragMode"
                            data-option="move"
                            title="Move">
            <span class="docs-tooltip" data-toggle="tooltip" title="First Image">
              <span class="fa fa-fast-backward"></span>
            </span>
                    </button>
                    <button type="button" class="btn btn-primary left-click" data-method="setDragMode"
                            data-option="move"
                            title="Move">
            <span class="docs-tooltip" data-toggle="tooltip" title="Previous Image">
              <span class="fa fa-arrow-left"></span>
            </span>
                    </button>
                    <button type="button" class="btn btn-primary right-click" data-method="setDragMode"
                            data-option="move"
                            title="Move">
            <span class="docs-tooltip" data-toggle="tooltip" title="Next Image">
              <span class="fa fa-arrow-right"></span>
            </span>
                    </button>
                    <button type="button" class="btn btn-primary forward-click" data-method="setDragMode"
                            data-option="move"
                            title="Move">
            <span class="docs-tooltip" data-toggle="tooltip" title="Last Image">
              <span class="fa fa-fast-forward"></span>
            </span>
                    </button>
                    <button type="button" class="btn btn-primary" data-method="setDragMode" data-option="move"
                            title="Move">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.setDragMode(&quot;move&quot;)">
              <span class="fa fa-arrows-alt"></span>
            </span>
                    </button>
                    <button type="button" class="btn btn-primary" data-method="setDragMode" data-option="crop"
                            title="Crop">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.setDragMode(&quot;crop&quot;)">
              <span class="fa fa-crop-alt"></span>
            </span>
                    </button>
                </div>

                <div class="btn-group">
                    <button type="button" class="btn btn-primary" data-method="zoom" data-option="0.1"
                            title="Zoom In">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.zoom(0.1)">
              <span class="fa fa-search-plus"></span>
            </span>
                    </button>
                    <button type="button" class="btn btn-primary" data-method="zoom" data-option="-0.1"
                            title="Zoom Out">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.zoom(-0.1)">
              <span class="fa fa-search-minus"></span>
            </span>
                    </button>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-primary" data-method="rotate" data-option="-1"
                            title="Rotate Left">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.rotate(-1)">
              <span class="fa fa-undo-alt"></span>
            </span>
                    </button>
                    <button type="button" class="btn btn-primary" data-method="rotate" data-option="1"
                            title="Rotate Right">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.rotate(1)">
              <span class="fa fa-redo-alt"></span>
            </span>
                    </button>
                </div>


                <div class="btn-group">
                    <button type="button" class="btn btn-primary" data-method="reset" title="Reset">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.reset()">
              <span class="fa fa-sync-alt"></span>
            </span>
                    </button>


                </div>

                <div class="btn-group btn-group-crop">
                    <button type="button" class="btn btn-success" data-method="getCroppedCanvas"
                            data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }">
            <span class="docs-tooltip" data-toggle="tooltip"
                  title="cropper.getCroppedCanvas({ maxWidth: 4096, maxHeight: 4096 })">
              Get Cropped Canvas
            </span>
                    </button>
                </div>

                <!-- Show the cropped image in modal -->
                <div class="modal fade docs-cropped" id="getCroppedCanvasModal" role="dialog" aria-hidden="true"
                     aria-labelledby="getCroppedCanvasTitle" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="getCroppedCanvasTitle">Cropped</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body"></div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                </button>
                                <a class="btn btn-primary" id="download" href="javascript:void(0);"
                                   download="cropped.jpg">Download</a>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal -->


            </div><!-- /.docs-buttons -->

            <div class="col-md-3 docs-toggles">
                <!-- <h3>Toggles:</h3> -->


                <div class="dropdown dropup docs-options">

                    <ul class="dropdown-menu" role="menu" aria-labelledby="toggleOptions">
                        <li class="dropdown-item">
                            <div class="form-check">
                                <input class="form-check-input" id="responsive" type="checkbox"
                                       name="responsive"
                                       checked>
                                <label class="form-check-label" for="responsive">responsive</label>
                            </div>
                        </li>
                        <li class="dropdown-item">
                            <div class="form-check">
                                <input class="form-check-input" id="restore" type="checkbox" name="restore"
                                       checked>
                                <label class="form-check-label" for="restore">restore</label>
                            </div>
                        </li>
                        <li class="dropdown-item">
                            <div class="form-check">
                                <input class="form-check-input" id="checkCrossOrigin" type="checkbox"
                                       name="checkCrossOrigin" checked>
                                <label class="form-check-label" for="checkCrossOrigin">checkCrossOrigin</label>
                            </div>
                        </li>
                        <li class="dropdown-item">
                            <div class="form-check">
                                <input class="form-check-input" id="checkOrientation" type="checkbox"
                                       name="checkOrientation" checked>
                                <label class="form-check-label" for="checkOrientation">checkOrientation</label>
                            </div>
                        </li>
                        <li class="dropdown-item">
                            <div class="form-check">
                                <input class="form-check-input" id="modal" type="checkbox" name="modal" checked>
                                <label class="form-check-label" for="modal">modal</label>
                            </div>
                        </li>
                        <li class="dropdown-item">
                            <div class="form-check">
                                <input class="form-check-input" id="guides" type="checkbox" name="guides"
                                       checked>
                                <label class="form-check-label" for="guides">guides</label>
                            </div>
                        </li>
                        <li class="dropdown-item">
                            <div class="form-check">
                                <input class="form-check-input" id="center" type="checkbox" name="center"
                                       checked>
                                <label class="form-check-label" for="center">center</label>
                            </div>
                        </li>
                        <li class="dropdown-item">
                            <div class="form-check">
                                <input class="form-check-input" id="highlight" type="checkbox" name="highlight"
                                       checked>
                                <label class="form-check-label" for="highlight">highlight</label>
                            </div>
                        </li>
                        <li class="dropdown-item">
                            <div class="form-check">
                                <input class="form-check-input" id="background" type="checkbox"
                                       name="background"
                                       checked>
                                <label class="form-check-label" for="background">background</label>
                            </div>
                        </li>
                        <li class="dropdown-item">
                            <div class="form-check">
                                <input class="form-check-input" id="autoCrop" type="checkbox" name="autoCrop"
                                       checked>
                                <label class="form-check-label" for="autoCrop">autoCrop</label>
                            </div>
                        </li>
                        <li class="dropdown-item">
                            <div class="form-check">
                                <input class="form-check-input" id="movable" type="checkbox" name="movable"
                                       checked>
                                <label class="form-check-label" for="movable">movable</label>
                            </div>
                        </li>
                        <li class="dropdown-item">
                            <div class="form-check">
                                <input class="form-check-input" id="rotatable" type="checkbox" name="rotatable"
                                       checked>
                                <label class="form-check-label" for="rotatable">rotatable</label>
                            </div>
                        </li>
                        <li class="dropdown-item">
                            <div class="form-check">
                                <input class="form-check-input" id="scalable" type="checkbox" name="scalable"
                                       checked>
                                <label class="form-check-label" for="scalable">scalable</label>
                            </div>
                        </li>
                        <li class="dropdown-item">
                            <div class="form-check">
                                <input class="form-check-input" id="zoomable" type="checkbox" name="zoomable"
                                       checked>
                                <label class="form-check-label" for="zoomable">zoomable</label>
                            </div>
                        </li>
                        <li class="dropdown-item">
                            <div class="form-check">
                                <input class="form-check-input" id="zoomOnTouch" type="checkbox"
                                       name="zoomOnTouch"
                                       checked>
                                <label class="form-check-label" for="zoomOnTouch">zoomOnTouch</label>
                            </div>
                        </li>
                        <li class="dropdown-item">
                            <div class="form-check">
                                <input class="form-check-input" id="zoomOnWheel" type="checkbox"
                                       name="zoomOnWheel"
                                       checked>
                                <label class="form-check-label" for="zoomOnWheel">zoomOnWheel</label>
                            </div>
                        </li>
                        <li class="dropdown-item">
                            <div class="form-check">
                                <input class="form-check-input" id="cropBoxMovable" type="checkbox"
                                       name="cropBoxMovable"
                                       checked>
                                <label class="form-check-label" for="cropBoxMovable">cropBoxMovable</label>
                            </div>
                        </li>
                        <li class="dropdown-item">
                            <div class="form-check">
                                <input class="form-check-input" id="cropBoxResizable" type="checkbox"
                                       name="cropBoxResizable" checked>
                                <label class="form-check-label" for="cropBoxResizable">cropBoxResizable</label>
                            </div>
                        </li>
                        <li class="dropdown-item">
                            <div class="form-check">
                                <input class="form-check-input" id="toggleDragModeOnDblclick" type="checkbox"
                                       name="toggleDragModeOnDblclick" checked>
                                <label class="form-check-label"
                                       for="toggleDragModeOnDblclick">toggleDragModeOnDblclick</label>
                            </div>
                        </li>
                    </ul>
                </div><!-- /.dropdown -->


            </div><!-- /.docs-toggles -->
        </div>

    </div>


    <div style="width:1000px;margin:80px auto;">

        <div id="thumbnail-slider" style="float:left;">
            <div class="inner">
                <?php $count = 0?>
                <ul>
                    @foreach($images as $each)

                        <li class="slider-click" index="{{$count++}}">
                            {{--<img src="{{asset('images/'.str_replace('-', '', $uploads->date).'/'.$uploads->publication.'/'.$each->image)}}"--}}
                            {{--width="100%" height="90%" style="margin-top: -16px">--}}
                            <a class="thumb"
                               href="{{asset('images/'.str_replace('-', '', $upload->date).'/'.$upload->publication.'/'.$each->image)}}"></a>
                        </li>
                    @endforeach


                </ul>
            </div>
        </div>
        <div id="ninja-slider" style="float:right;">

            <div class="slider-inner">
                <div>
                    <!-- Wrapper for slides -->
                    <div>
                        <div class="" style="width:1%; height:280px;margin-top: 206px; position: absolute ">
                            <a href="javascript:;" class="left_arrow_pgright btn_prev left-click"
                               style="text-decoration:none;"
                               title="Previous" alt="Previous"> <span> <i class="fa fa-angle-left"
                                                                          style="font-size: 50px"></i> </span> </a>
                        </div>
                        <div class=""
                             style=" width:1%; height:280px;padding-left: 10px;margin-top: 206px;left: 767px; position: absolute">
                            <a href="javascript:;" class="left_arrow_pgright btn_prev right-click"
                               style="text-decoration:none;"
                               title="Previous" alt="Previous"> <span> <i class="fa fa-angle-right"
                                                                          style="font-size: 50px"></i> </span> </a>
                        </div>
                        <div class="carousel-inner frame" style="float:left; width:94%; margin-left: 24px;">
                            <div class="item active img-container cropper-preview" id="cropper-preview">
                                <img id="image-cropper"
                                     src="{{asset('images/'.str_replace('-', '', $upload->date).'/'.$upload->publication.'/'.$image->image)}}"

                                     style="margin-top: -16px">
                                {{--<img src="http://localhost:8000/images/20191209/1/3.JPG" alt="Los Angeles" style="width:100%;"--}}
                                {{--class="">--}}
                            </div>


                        </div>

                    </div>
                    <!-- Left and right controls -->
                </div>
            </div>
        </div>
    </div>



    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"
            crossorigin="anonymous"></script>
    <script src="https://fengyuanchen.github.io/shared/google-analytics.js" crossorigin="anonymous"></script>
    {{--<script src="https://fengyuanchen.github.io/cropperjs/js/cropper.js"></script>--}}
    <script src="{{asset('addedJs/cropper.js')}}"></script>
    <script src="https://fengyuanchen.github.io/cropperjs/js/main.js"></script>

    <script>
        var list = [];
        var count = 0;
        $(document).ready(function () {
            var base_url = window.location.origin;
            var images = $('meta[name=images]').attr('content');
            var date = $('meta[name=date]').attr('content').replace(/-/g, '');
            var publication = $('meta[name=publication]').attr('content');
            JSON.parse(images).forEach(function (each) {
                var url = base_url + '/public/images/' + date + '/' + publication + '/' + each.image;
                list.push(url);
            });
            $('#ninja-slider-prev').on('click', function () {
                alert();
            });
            $('#ninja-slider-next').on('click', function () {
                alert('ninja-slider-next');
            });
        });


        $('.left-click').on('click', function () {
            var image = document.getElementById('image-cropper');
            var cropper = new Cropper(image);
            cropper.destroy();
            // cropper.cropper('replace', 'http://pr.cyfous.in/public/images/20191216/1/hindu_21.09_3.jpg');

            var blob = new Blob;
            var imag = document.createElement("img");
            var result = document.getElementById('cropper-preview');

            var reader = new FileReader();
            reader.onload = function (e) {
                var img = document.createElement("img");
                img.id = "image-cropper";
                img.src = 'http://pr.cyfous.in/public/images/20191216/1/hindu_21.09_3.jpg';
                img.src = list[count];
                result.innerHTML = "";
                result.appendChild(img);

                img.addEventListener('zoom', (event) => {
                    // Zoom in
                    if (event.detail.ratio > event.detail.oldRatio) {
                        return this.zoomTo(20 * 1.2 / 10, null, event);
                    }
                });
                cropper = new Cropper(img);
                cropper.render();
                // }
            };
            // reader.readAsDataURL(e.target.files[0]);
            fetch('http://pr.cyfous.in/public/images/20191216/1/hindu_21.09_3.jpg').then(function (response) {
                return response.blob();

            }).then(function (myBlob) {
                reader.readAsDataURL(myBlob);
            });

            // }
            // image.replace('dddddddd')

            // var tmpImg = new Image();
            // tmpImg.src = list[count]; //or  document.images[i].src;
            // tmpImg.src = list[count]; //or  document.images[i].src;
            // $(tmpImg).one('load', function () {
            //     orgWidth = tmpImg.width;
            //     orgHeight = tmpImg.height;
            //     $('.cropper-preview').find('.cropper-hide').parent().find('img').remove();
            //     console.log(tmpImg, 'tmpImgtmpImgtmpImgtmpImgtmpImgtmpImgtmpImgtmpImg')
            //     $('.cropper-preview').append(tmpImg);
            //     // $('.cropper-preview').find('.cropper-hide').attr('src', list[count]);
            //     // $('.cropper-preview').find('.cropper-hide').css('height', orgHeight);
            //     // $('.cropper-preview').find('.cropper-hide').css('width', orgWidth);
            //     $('.cropper-preview').find('.cropper-view-box').children('img').attr('src', list[count]);
            //     // $('.cropper-preview').find('.cropper-view-box').children('img').css('height', orgHeight);
            //     // $('.cropper-preview').find('.cropper-view-box').children('img').css('width', orgWidth);
            // });

            // $('.cropper-preview').find('.cropper-hide').attr('src', list[count]);
            // $('.cropper-preview').find('.cropper-view-box').children('img').attr('src', list[count]);

            if (list.length - 1 != count)
                count++;
            else
                count = 0;
        });

        $('.right-click').on('click', function () {

            $('.cropper-preview').find('.cropper-hide').attr('src', list[count]);
            $('.cropper-preview').find('.cropper-view-box').children('img').attr('src', list[count]);
            if (count != 0)
                count--;
            else
                count = list.length - 1;
        });
        $('.backward-click').on('click', function () {
            count = 0;
            $('.cropper-preview').find('.cropper-hide').attr('src', list[count]);
            $('.cropper-preview').find('.cropper-view-box').children('img').attr('src', list[count]);


        })
        $('.forward-click').on('click', function () {
            count = list.length - 1;
            $('.cropper-preview').find('.cropper-hide').attr('src', list[count]);
            $('.cropper-preview').find('.cropper-view-box').children('img').attr('src', list[count]);

        })
        $('.slider-click').on('click', function () {
            count = $(this).attr('index');
            $('.cropper-preview').find('.cropper-hide').attr('src', list[count]);
            $('.cropper-preview').find('.cropper-view-box').children('img').attr('src', list[count]);
        })
    </script>

@endsection