@extends('layouts.app')
@section('content')

    <!-- Start DropZone Cropperjs -->
    <meta name="deadline" content="{{ $images }}">
    <meta name="date" content="{{ $uploads->date }}">
    <meta name="publication" content="{{ $uploads->publication }}">
    <meta name="url" content="{{ $url }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.2.0/min/dropzone.min.css"
          rel="stylesheet"/>
    <link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
          rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.3/cropper.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.2.0/min/dropzone.min.js"></script>
    <script src="https://unpkg.com/cropperjs.js"></script>
    <!-- End DropZone Cropperjs -->

    <!-- Start Selectable -->
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/pepper-grinder/jquery-ui.css"
          rel="stylesheet"/>
    <link rel="stylesheet"
          href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <link href="{{asset('customCss/customListSelection.css')}}" rel="stylesheet"/>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- End Selectable -->



    <div class="row">
        <div class="col-md-12" id="dropzoneVisibility" style="display: none">
            <div>

                <form class="dropzone" id="my-dropzone" style="border: 2px dashed #0087F7 ;border-radius: 5px;">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{$uploads->id}}" name="id">
                    <input type="hidden" name="publication" id="dropzonePublication" value="{{$uploads->publication}}">
                    <input type="hidden" name="date" id="dropzoneDate" value="{{$uploads->date}}">
                </form>
                <button type="button" class="btn btn-info" id="submit-all">Upload</button>
                {{--<a href="{{url('list/'.$uploads->id)}}">--}}
                {{--<button type="button" class="btn btn-info" id="listEdit">Close</button>--}}
                {{--</a>--}}

            </div>
        </div>
        <div id="listImagesVisibility" style="width: 100%">
            <div class="card">
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{url('search')}}">
                            <button class="btn btn-danger" style="margin-left:12px ">Back</button>
                        </a>
                        <div class="pull-right" style="margin-right: 10px">

                            {{--@if (auth()->user()->is_admin == 1)--}}
                            {{--<button class="btn btn-success" id="dropzoneEdit">Edit</button>--}}
                            {{--@endif--}}

                            <button class="btn btn-primary " id="export">Export as PDF</button>
                            <button id="selected" class="btn  ">Download as JPG</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card demo-icons">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-body all-icons">
                            <div class="icons-wrapper">

                                <section>

                                    <ul id="selectable">
                                        @foreach($images as $each)
                                            <li class="corp imageHover" style="width:228px">
                                                {{--<button style="z-index: 9999;position: absolute;top: 50px;left: 61px;"--}}
                                                {{--class="btn btn-info">--}}
                                                {{--Crop--}}
                                                {{--</button>--}}
                                                {{--<i class="fas fa-camera fa-xs"></i>--}}
                                                {{--<a href="{{url('crop/'.$each->id)}}">--}}
                                                <button class="crop-image btn btn-warning" data-value="{{$each->id}}"
                                                        style="z-index:9999999;position:absolute;margin-top: 140px;margin-left:-35px;">
                                                    Crop
                                                </button>
                                                {{--<i class="fa fa-crop fa-2x" data-value="{{$uploads->id}}"--}}
                                                {{--style="z-index: 9999;position: absolute; width: 40px;height: 40px;top: 17px"></i>--}}
                                                {{--</a>--}}

                                                <div style="height: 347px; width: 228px">
                                                    {{--                                                    <img src="{{asset('images/'.str_replace('-', '', $uploads->date).'/'.$uploads->publication.'/'.$each->image)}}"--}}
                                                    <img src="{{$url.'images/'.str_replace('-', '', $uploads->date).'/'.$uploads->publication.'/'.$each->image}}"
                                                         width="100%" height="90%" style="margin-top: -16px">
                                                    <span>{{$each->image}}</span>
                                                </div>
                                            </li>

                                        @endforeach

                                    </ul>
                                </section>


                            </div>


                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
    <script src='//js.zapjs.com/js/download.js'></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>


    <script>
        var successCount = false;


        $(document).ready(function () {
            var base_url = $('meta[name=url]').attr('content');
            var local_url = window.location.origin;
            var images = $('meta[name=deadline]').attr('content');
// var date = $('meta[name=date]').attr('content').replace('-', '');
            var date = $('meta[name=date]').attr('content').replace(/-/g, '');
            var publication = $('meta[name=publication]').attr('content');

            Dropzone.options.myDropzone = {
                autoProcessQueue: false,
                uploadMultiple: true,
                acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
                init: function () {

                    myDropzone = this;
                    var getFileBlob = function (url, cb) {
                        var xhr = new XMLHttpRequest();
                        xhr.open("GET", url);
                        xhr.responseType = "blob";
                        xhr.addEventListener('load', function () {
                            cb(xhr.response);
                        });
                        xhr.send();
                    };

                    var getFileObject = function (filePathOrUrl, cb) {
                        getFileBlob(filePathOrUrl, function (blob) {
                            cb(blobToFile(blob, 'test.jpg'));
                        });
                    };

                    JSON.parse(images).forEach(function (item) {
                        var url = base_url + 'images/' + date + '/' + publication + '/' + item.image;
                        // var url = base_url + '/images/' + date + '/' + publication + '/' + item.image;
                        getFileObject(url, function (fileObject) {

                            console.log(fileObject)
                            var mockFile = blobToFile(fileObject, item.image);
                            mockFile.dataURL = url;
                            myDropzone.emit("addedfile", mockFile);
                            myDropzone.emit("thumbnail", mockFile, url);
                            myDropzone.emit("complete", mockFile);
                            mockFile.accepted = true;
                            console.log(mockFile)
                            myDropzone.files.push(mockFile);
                        });
                    });

                    var submitButton = document.querySelector('#submit-all');
                    submitButton.addEventListener("click", function () {
                        myDropzone.processQueue();
                    });
                    this.on("complete", function () {
                        if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
                            var _this = this;
                            _this.removeAllFiles();
                        }
                        // list_image();
                    });
                },
            };

            var Cropper = window.Cropper;
            Dropzone.autoDiscover = false;
            var c = 0;
            var cropped = false;

            var myDropzone = new Dropzone('#my-dropzone', {
                // url: local_url + '/public/save',
                url: local_url + '/save',
                addRemoveLinks: true,
                parallelUploads: 50,
                uploadMultiple: true,
                createImageThumbnails: true,
                autoProcessQueue: false,
                maxFiles: 50,
                maxFilesize: 100,
                maxThumbnailFilesize: 40,
                timeout: 100000,
                acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
                success: function (file, response) {
                    successCount = true;
                },
                queuecomplete: function () {
                    if (successCount)
                        demo.showNotification('top', 'right');
                }
            });

            $('.button-click').on('click', function () {
                if (!cropped) {
                    myDropzone.removeFile(file);
                    cropper(file);
                } else {
                    cropped = false;
                    var previewURL = URL.createObjectURL(file);
                    var dzPreview = $(file.previewElement).find('img');
                    dzPreview.attr("src", previewURL);
                }
            })

            myDropzone.on('addedfile', function (file) {
                file.previewElement.addEventListener("click", function () {
                    // myDropzone.removeFile(file);
                    cropper(file);
                });
            });

            var cropper = function (file) {
                var fileName = file.name;
                console.log(file, 'file')
                var loadedFilePath = getSrcImageFromBlob(file);
                // @formatter:off
                var modalTemplate =
                    '<div class="modal fade" tabindex="-1" role="dialog">' +
                    '<div class="modal-dialog modal-xl "  role="document">' +
                    '<div class="modal-content" >' +
                    '<div class="modal-header">' +
                    '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>' +
                    '</div>' +
                    '<div class="modal-body">' +
                    '<div class="cropper-container">' +
                    '<img id="img-' + ++c + '" src="' + loadedFilePath + '" data-vertical-flip="false" data-horizontal-flip="false">' +
                    '</div>' +
                    '</div>' +
                    '<div class="modal-footer">' +
                    '<button type="button" class="btn btn-warning rotate-left"><span class="fa fa-rotate-left"></span></button>' +
                    '<button type="button" class="btn btn-warning rotate-right"><span class="fa fa-rotate-right"></span></button>' +
                    // '<button type="button" class="btn btn-warning scale-x" data-value="-1"><span class="fa fa-arrows-h"></span></button>' +
                    // '<button type="button" class="btn btn-warning scale-y" data-value="-1"><span class="fa fa-arrows-v"></span></button>' +
                    '<button type="button" class="btn btn-warning reset"><span class="fa fa-refresh"></span></button>' +
                    '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
                    '<button type="button" class="btn btn-primary crop-upload">Crop & upload</button>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
                // @formatter:on

                var $cropperModal = $(modalTemplate);

                $cropperModal.modal('show').on("shown.bs.modal", function () {
                    var $image = $('#img-' + c);
                    var cropper = $image.cropper({
                        autoCropArea: 1,
                        // aspectRatio: 9 / 16,
                        cropBoxResizable: true,
                        movable: true,
                        rotatable: true,
                        scalable: true,
                        viewMode: 2,
                        minContainerWidth: 250,
                        maxContainerWidth: 250,
                        zoomOnWheel: true
                    }).on('hidden.bs.modal', function () {
                        $image.cropper('destroy');
                    });

                    $cropperModal.on('click', '.crop-upload', function () {
                        // get cropped image data
                        var cropp = $image.cropper('getCroppedCanvas', {
                            width: 90,
                            height: 90,
                            minWidth: 256,
                            minHeight: 256,
                            maxWidth: 4096,
                            maxHeight: 4096,
                            fillColor: '#fff',
                            imageSmoothingEnabled: false,
                            imageSmoothingQuality: 'high'
                        });
                        cropp.toBlob(function (blob) {
                            myDropzone.createThumbnail(
                                blob,
                                myDropzone.options.thumbnailWidth,
                                myDropzone.options.thumbnailHeight,
                                myDropzone.options.thumbnailMethod,
                                true,
                                function (dataURL) {
                                    console.log(dataURL, 'dataUrl2')
                                    var croppedFile = blobToFile(blob, fileName);
                                    croppedFile.accepted = true;
                                    // Update the Dropzone file thumbnail
                                    myDropzone.emit('thumbnail', croppedFile, dataURL);
                                    // var croppedFile = blobToFile(blob, fileName);
                                    // Return the file to Dropzone
                                    // myDropzone.emit('addedfile', file);
                                    // myDropzone.files.push(blob);
                                    // done(blob);
                                });
                            var croppedFile = blobToFile(blob, fileName);
                            croppedFile.accepted = true;
                            var files = myDropzone.getAcceptedFiles();
                            for (var i = 0; i < files.length; i++) {
                                var file = files[i];
                                if (file.name === fileName) {
                                    myDropzone.removeFile(file);
                                }
                            }
                            cropped = true;
                            myDropzone.files.push(croppedFile);
                            myDropzone.emit('addedfile', croppedFile);
                        });
                        $cropperModal.modal('hide');
                    })
                        .on('click', '.rotate-right', function () {
                            $image.cropper('rotate', 10);
                        })
                        .on('click', '.rotate-left', function () {
                            $image.cropper('rotate', -10);
                        })
                        .on('click', '.reset', function () {
                            $image.cropper('reset');
                        })
                });
            };

            function getSrcImageFromBlobs(blob) {
                var urlCreator = window.URL || window.webkitURL;
                return urlCreator.createObjectURL(blob);
            }

            function getSrcImageFromBlob(object) {
                return (window.URL) ? window.URL.createObjectURL(object) : window.webkitURL.createObjectURL(object);
            }

            function blobToFile(theBlob, fileName) {
                theBlob.lastModifiedDate = new Date();
                theBlob.name = fileName;
                theBlob.upload = {'filename': fileName}
                theBlob.status = "queued";
                return theBlob;
            }
        });


    </script>


    <script>

        var $currentlySelected = null;
        var selected = [];

        $('#selectable').selectable({
            start: function (event, ui) {
                $currentlySelected = $('#selectable .ui-selected');
            },
            stop: function (event, ui) {
                for (var i = 0; i < selected.length; i++) {
                    if ($.inArray(selected[i], $currentlySelected) >= 0) {
                        $(selected[i]).removeClass('ui-selected');

                    }
                }
                selected = [];
            },
            selecting: function (event, ui) {
                $currentlySelected.addClass('ui-selected'); // re-apply ui-selected class to currently selected items
            },
            selected: function (event, ui) {
                console.log(event, 'eventeventeventeventevent')
                selected.push(ui.selected);
                // $currentlySelected.closest('li').find('i').css('display', 'block');

            }
        });
        $('.crop-image').on('click', function () {
            var id = $(this).attr('data-value');
            window.location.replace(window.location.origin + "/public/crop/" + id);
        })

        $('#selected').on('click', function () {
            var elements = document.getElementsByClassName("ui-selected");
            $.each(elements, function (index, value) {
                $(".ui-selected").each(function () {
                    var a = document.createElement('a');
                    var url = $(this).find('img').attr('src');
                    a.href = url;
                    var res = url.split("/");
                    a.download = res[res.length - 1];
                    document.body.appendChild(a);
                    a.click();
                    document.body.removeChild(a);
                });

            });
            $(".ui-selected").removeClass("ui-selected");
        });


        $('#dropzoneEdit').on('click', function () {
            successCount = true;
            $('#listImagesVisibility').hide();
            $('#dropzoneVisibility').show();

        });
        var createPDF = function (imgData) {
            var doc = new jsPDF('p', 'pt', 'a4');
            var width = doc.internal.pageSize.width;
            var height = doc.internal.pageSize.height;
            var options = {
                pagesplit: true
            };
            var count = 1;
            var length = imgData.length;
            imgData.forEach(function (element) {
                var img = new Image();
                img.src = element;
                console.log('imgimgimgimgimgimgimg', img);
                // doc.text(10, 20, 'Crazy Monkey');
                var h1 = 50;
                var aspectwidth1 = (height - h1) * (9 / 16);
                doc.addImage(img, 'JPEG', 10, h1, aspectwidth1, (height - h1), '');
                if (count != length)
                    doc.addPage();
                count++;
            });
            doc.output('datauri');
            if (length)
                doc.save("Export.pdf");

        }

        function toDataURL(url, callback) {
            var xhr = new XMLHttpRequest();
            xhr.onload = function () {
                var reader = new FileReader();
                reader.onloadend = function () {
                    callback(reader.result);
                };
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.responseType = 'blob';
            xhr.send();
        }

        $('#export').on('click', function () {
            var urlArray = [];
            $(".ui-selected").each(function () {
                var url = $(this).find('img').attr('src');
                var docs = [];
                urlArray.push(url)
                // var doc = new jsPDF();
                // var ssss = toDataURL(url.toLowerCase(), function (dataUrl) {
                //     var dataURL = "data:image/jpg;base64," + dataUrl;
                //     console.log(dataUrl, 'dataUrl');
                //     doc.addImage(dataUrl, 'JPG', 15, 40, 180, 180);
                //     // doc.save("Export.pdf");
                //     return 'ssss'
                // })
            });
            // console.log(urlArray)
            createPDF(urlArray);

        });
    </script>


@endsection