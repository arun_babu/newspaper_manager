@extends('layouts.app')
@section('content')
    <div class="wrapper wrapper-full-page ">
        <div>
            <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
            <div class="content">
                <div class="container">
                    <div class="col-lg-6 col-md-8 ml-auto mr-auto">
                        <form method="POST" action="{{  url('users/store') }}">
                            @csrf
                            <input name="id" value="{{$user->id}}" type="hidden">
                            <div class="card card-login">
                                <div class="card-header ">
                                    <div class="card-header ">
                                        <h3 class="header text-center">Update</h3>
                                    </div>
                                </div>
                                <div class="card-body ">
                                    <div class="form-group row">
                                        <label for="name"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text"
                                                   class="form-control @error('name') is-invalid @enderror"
                                                   name="name" value="{{ old('name')?old('name'):$user->name }}"
                                                   required autocomplete="name"
                                                   autofocus>

                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="email"
                                               class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email"
                                                   class="form-control @error('email') is-invalid @enderror"
                                                   name="email" value="{{ old('email')?old('email'):$user->email }}"
                                                   required
                                                   autocomplete="email">

                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="password-confirm"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>

                                        <div class="col-md-6">
                                            <input type="text"
                                                   class="form-control @error('mobile_no') is-invalid @enderror"
                                                   name="mobile_no"
                                                   value="{{ old('mobile_no')?old('mobile_no'):$user->mobile_no }}">
                                            @error('mobile_no')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    {{--<div class="form-group row">--}}
                                    {{--<label for="password"--}}
                                    {{--class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

                                    {{--<div class="col-md-6">--}}
                                    <input id="password" type="hidden"
                                           class="form-control @error('password') is-invalid @enderror"
                                           name="password"
                                           required autocomplete="new-password"
                                           value="123234566">

                                    {{--@error('password')--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                    {{--<strong>{{ $message }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@enderror--}}
                                    {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="form-group row">--}}
                                    {{--<label for="password-confirm"--}}
                                    {{--class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>--}}

                                    {{--<div class="col-md-6">--}}
                                    <input id="password-confirm" type="hidden" class="form-control" value="123234566"
                                           name="password_confirmation" required autocomplete="new-password">
                                    {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                Update
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection