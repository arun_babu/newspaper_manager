@extends('layouts.app')
@section('content')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link href="{{asset('customCss/customListSelection.css')}}" rel="stylesheet"/>
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                '../../../../www.googletagmanager.com/gtm5445.html?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NKDMSK6');
    </script>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="toolbar">
                        <!--        Here you can write extra buttons/actions for the toolbar              -->
                    </div>
                    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="disabled-sorting" style="width: 10px">#</th>
                            <th>User Name</th>
                            <th>Mobile</th>
                            <th class="disabled-sorting text-right" style="width: 10px">Edit</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php $i = 0;?>
                        @foreach($users as $each)
                            @if(Auth::user()->id!=$each->id)
                                <tr>
                                    <td data-orderable="false">{{++$i}}</td>
                                    <td>
                                        <p>{{$each->name}}</p>
                                    </td>
                                    <td><p>{{$each->mobile_no}}</p></td>
                                    <td class="">
                                        <a href="{{url('users/'. $each->id)}}"
                                           class="btn btn-primary btn-round btn-icon"><i
                                                    {{--class="btn btn-info btn-link btn-icon btn-lg like"><i--}}
                                                    class="fa fa-eye"></i></a>

                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- end content-->
            </div>
            <!--  end card  -->
        </div>
        <!-- end col-md-12 -->
    </div>

    {{--<div class="card demo-icons">--}}
    {{--<div class="card-body all-icons">--}}
    {{--<div id="icons-wrapper">--}}
    {{--<section>--}}
    {{--<ul>--}}
    {{--@foreach($uploads as $each)--}}
    {{--<li>--}}
    {{--<a href="{{url('list/'. $each->id)}}">--}}
    {{--<i class="nc-icon nc-single-copy-04"></i>--}}
    {{--<p>{{$each->getPublicationById->name}}</p>--}}
    {{--</a>--}}
    {{--</li>--}}
    {{--@endforeach--}}
    {{--</ul>--}}
    {{--</section>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <script src="{{asset('/js/plugins/moment.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#datatable').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                }

            });
        });
    </script>
    <script type="text/javascript">
        $(function () {

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
                $('#start').val(start.format('M/D/YYYY'));
                $('#end').val(end.format('M/D/YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);

        });
    </script>
    <script>
        $(document).ready(function () {
            // initialise Datetimepicker and Sliders
            demo.initDateTimePicker();
            if ($('.slider').length != 0) {
                demo.initSliders();
            }
        });
    </script>
    <script src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
@endsection