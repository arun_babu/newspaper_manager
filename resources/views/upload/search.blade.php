@extends('layouts.app')
@section('content')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link href="{{asset('customCss/customListSelection.css')}}" rel="stylesheet"/>
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                '../../../../www.googletagmanager.com/gtm5445.html?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NKDMSK6');
    </script>

    <div class="card">
        <form method="get" action="{{url('/search')}}">
            <div class="row">

                <div class="col-md-3">
                    <div class="card-body " style="width: 340px">
                        <div class="form-group">
                            {{--<input type="text" class="form-control datepicker" value="{{$date}}" name="date">--}}
                            {{--<input type="text" name="date" class="form-control "/>--}}
                            <div id="reportrange"
                                 style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 75%">
                                <i class="fa fa-calendar"></i>&nbsp;
                                <span></span> <i class="fa fa-caret-down"></i>
                            </div>
                            <input name="start" id="start" type="hidden"/>
                            <input name="end" id="end" type="hidden"/>

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-body ">
                        <div class="form-group">
                            <select class="browser-default custom-select " id="publication"
                                    name="publication">
                                <option value="null" selected>All</option>
                                @foreach($publications as $each)
                                    <option value="{{$each->id}}">{{$each->name}}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" id="confirm" style="margin-top: 15px;">Search
                        </button>

                    </div>
                </div>
            </div>
        </form>

    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="toolbar">
                        <!--        Here you can write extra buttons/actions for the toolbar              -->
                    </div>
                    <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="disabled-sorting" style="width: 10px">#</th>
                            <th>Daily</th>
                            <th>Date</th>
                            <th class="disabled-sorting text-right" style="width: 10px">View</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php $i = 0;?>
                        @foreach($uploads as $each)
                            <tr>
                                <td data-orderable="false">{{++$i}}</td>
                                <td>
                                    <p>{{$each->getPublicationById->name}}</p>
                                </td>
                                <td><p>{{ Carbon\Carbon::parse($each->date)->format('M d, Y')}}</p></td>
                                <td class="">
                                    <a href="{{url('list/'. $each->id)}}"
                                       class="btn btn-primary btn-round btn-icon"><i
                                                {{--class="btn btn-info btn-link btn-icon btn-lg like"><i--}}
                                                class="fa fa-eye"></i></a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- end content-->
            </div>
            <!--  end card  -->
        </div>
        <!-- end col-md-12 -->
    </div>

    {{--<div class="card demo-icons">--}}
    {{--<div class="card-body all-icons">--}}
    {{--<div id="icons-wrapper">--}}
    {{--<section>--}}
    {{--<ul>--}}
    {{--@foreach($uploads as $each)--}}
    {{--<li>--}}
    {{--<a href="{{url('list/'. $each->id)}}">--}}
    {{--<i class="nc-icon nc-single-copy-04"></i>--}}
    {{--<p>{{$each->getPublicationById->name}}</p>--}}
    {{--</a>--}}
    {{--</li>--}}
    {{--@endforeach--}}
    {{--</ul>--}}
    {{--</section>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <script src="{{asset('/js/plugins/moment.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#datatable').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                }

            });
        });
    </script>
    <script type="text/javascript">
        $(function () {

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
                $('#start').val(start.format('M/D/YYYY'));
                $('#end').val(end.format('M/D/YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);

        });
    </script>
    <script>
        $(document).ready(function () {
            // initialise Datetimepicker and Sliders
            demo.initDateTimePicker();
            if ($('.slider').length != 0) {
                demo.initSliders();
            }
        });
    </script>
    <script src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>

@endsection
