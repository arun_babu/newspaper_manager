@extends('layouts.app')
@section('content')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="images" content="{{$images}}">
    <meta name="date" content="{{ $upload->date }}">
    <meta name="publication" content="{{ $upload->publication }}">
    <meta name="publicationName" content="{{$upload->getPublicationById->name}}">
    <meta name="selected" content="{{ $selected }}">
    <meta name="url" content="{{ $url }}">

    <title>jquery-cropper</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('crop/css/bootstrap.min.css')}}"
          crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('crop/css/cropper.css')}}" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('crop/css/main.css')}}">
    <link href="{{asset('slider/css/ninja-slider.css')}}" rel="stylesheet"/>
    <script src="{{asset('slider/js/ninja-slider.js')}}"></script>
    <link href="{{asset('slider/css/thumbnail-slider.css')}}" rel="stylesheet" type="text/css"/>
    <script src="{{asset('slider/js/thumbnail-slider.js')}}" type="text/javascript"></script>
    <style>
        .customColor {
            background-color: #4fb9e1;
            border-color: #4fb9e1;
        }

        .buttonRound {
            border-radius: 50%;
        }

        #buttonboxback {
            width: 94px;
            height: 40px;
            background-color: #4fb9e1;
            position: relative;
            margin-left: 40px;
            border-radius: 5px;
        }

        #buttonboxback:before {
            content: "";
            position: absolute;
            top: 0;
            left: -17px;
            border-style: solid;
            border-width: 20px 20px 20px 0;
            border-color: transparent #4fb9e1 transparent transparent;
        }

        #buttonboxback > a {
            display: block;
            text-decoration: none;
            text-align: center;
            color: #fff;
            font: 18px/40px Sans-Serif;
        }

        #buttonboxback:hover {
            background-color: lightblue;
        }

        #buttonboxback:hover:before {
            border-right-color: lightblue;
        }


        #buttonboxpage {
            width: 100px;
            height: 40px;
            background-color: #4fb9e1;
            position: relative;
            margin-left: 40px;
            border-radius: 5px;
        }

        #buttonboxpage:after {

            content: "";
            position: absolute;
            left: 147px;
            width: 0;
            height: 0;
            top: 4px;
            border-top: 13px solid transparent;
            border-left: 10px solid #fff;
            border-bottom: 13px solid transparent;
            /*content: "";*/
            /*position: absolute;*/
            /*top: 0;*/
            /*right: -17px;*/
            /*border-style: solid;*/
            /*border-width: 20px 20px 20px 0;*/
            /*border-color: transparent #4fb9e1 transparent transparent;*/
        }

        #buttonboxpage > a {
            display: block;
            text-decoration: none;
            text-align: center;
            color: #fff;
            font: 18px/40px Sans-Serif;
        }

        #buttonboxpage:hover {
            background-color: lightblue;
        }

        #buttonboxpage:hover:before {
            border-right-color: lightblue;
        }


        .point-btn {

            align-items: center;
            width: 100px;
            height: 40px;
            background: #4fb9e1;
            position: relative;
            -moz-border-radius: 3px 0 0 3px;
            -webkit-border-radius: 3px 0 0 3px;
            border-radius: 3px 0 0 3px;
            margin-left: 0px;
            margin-right: 12px;
        }

        .point-btn:before {
            content: "";
            position: absolute;
            left: 100px;
            width: 20px;
            height: 41px;
            border-top: 20px solid transparent;
            border-left: 18px solid #4fb9e1;
            border-bottom: 20px solid transparent;
        }

        /*.point-btn:after {*/
        /*content:"";*/
        /*position: absolute;*/
        /*left: 147px;*/
        /*width: 0;*/
        /*height: 0;*/
        /*top: 4px;*/
        /*border-top: 13px solid transparent;*/
        /*border-left: 10px solid #4fb9e1;*/
        /*border-bottom: 13px solid transparent;*/
        /*}*/
        .point-btn a {
            display: block;
            text-decoration: none;
            text-align: center;
            color: #fff;
            font: 18px/40px Sans-Serif;
        }

        .newClass:hover {
            background-color: transparent !important;
            border-color: transparent !important;
            color: #4fb9e1 !important;
        }

        .loader {

            height: 0;
            width: 0;
            padding: 15px;
            border: 6px solid #ccc;
            border-right-color: #888;
            border-radius: 22px;
            /* left, top and position just for the demo! */
            position: absolute;
            z-index: 9;
            left: 350px;
            top: 200px;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

    </style>

    <!-- Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 docs-buttons" style="left:5px">
                        <div class=" btn-group">
                            <div id="buttonboxback">
                                <a href="{{url('/list/'.$upload->id)}}">BACK</a>
                            </div>
                            {{--<a href="{{url('/list/'.$upload->id)}}" class="arrow">--}}
                            {{--<button type="button" class="btn btn-danger customColor " id="buttonboxback">--}}
                            {{--<span class="docs-tooltip">--}}
                            {{--Back--}}
                            {{--</span>--}}
                            {{--</button>--}}

                            {{--</a>--}}
                        </div>
                        <!-- <h3>Toolbar:</h3> -->
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary customColor" data-method="setDragMode"
                                    data-option="move"
                                    title="Move">
                            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false"
                                  title="Move"
                            >
                              <span class="fa fa-arrows-alt"></span>
                            </span>
                            </button>
                            <button type="button" class="btn btn-primary customColor" data-method="setDragMode"
                                    data-option="crop"
                                    title="Crop">
                                <span class="docs-tooltip" data-toggle="tooltip" data-animation="false"
                                      title="Crop">
                                  <span class="fa fa-crop-alt"></span>
                                </span>
                            </button>
                        </div>

                        {{--<div class="row">--}}
                        <button type="button" class="btn btn-primary customColor buttonRound" style="margin-top: 1px;
                        margin-left:-2px;"
                                data-method="zoom"
                                data-option="0.1"
                        >
                                 <span class="docs-tooltip" data-toggle="tooltip" data-animation="false"
                                       title="Zoom In">
                                  <span class="fa fa-search-plus"></span>
                                </span>
                        </button>

                        <button type="button" class="btn btn-primary customColor buttonRound" style="margin-top: 1px;
                        margin-left: -2px;"
                                data-method="zoom"
                                data-option="-0.1"
                        >
                                <span class="docs-tooltip" data-toggle="tooltip" data-animation="false"
                                      title="Zoom Out">
                                  <span class="fa fa-search-minus"></span>
                                </span>
                        </button>
                        {{--</div>--}}

                        {{--<div class="btn-group">--}}
                        {{--<button type="button" class="btn btn-primary customColor buttonRound" data-method="zoom"--}}
                        {{--data-option="0.1"--}}
                        {{--title="Zoom In">--}}
                        {{--<span class="docs-tooltip" data-toggle="tooltip" data-animation="false"--}}
                        {{--title="Zoom In">--}}
                        {{--<span class="fa fa-search-plus"></span>--}}
                        {{--</span>--}}
                        {{--</button>--}}
                        {{--<button type="button" class="btn btn-primary customColor buttonRound" data-method="zoom"--}}
                        {{--data-option="-0.1"--}}
                        {{--title="Zoom Out">--}}
                        {{--<span class="docs-tooltip" data-toggle="tooltip" data-animation="false"--}}
                        {{--title="Zoom Out">--}}
                        {{--<span class="fa fa-search-minus"></span>--}}
                        {{--</span>--}}
                        {{--</button>--}}
                        {{--</div>--}}

                        {{--<div class="btn-group">--}}
                        <button type="button" class="btn btn-primary customColor backward-click"
                                data-method="setDragMode"
                                data-option="move" title="Move"
                                style="border-radius: 8px; margin-top: 1px">
                            <span class="docs-tooltip" data-toggle="tooltip" title="First Image">
                                <span class="fa fa-fast-backward"></span>
                            </span>
                        </button>
                        <button type="button" class="btn btn-primary customColor left-click "
                                data-second-option="0" title="Previous Image"
                                style="margin-top: 1px; border-radius: 0px">
                            <span class="docs-tooltip" data-toggle="tooltip" title="Previous Image">
                                <span class="fa fa-arrow-left"></span>
                            </span>
                        </button>
                        <div class=" btn-group">
                            <div
                                    {{--id="buttonboxpage"--}}
                                    class="point-btn"
                            >
                                <a href="#">PAGE</a>
                            </div>
                        </div>
                        {{--<button type="button" class="btn btn-primary customColor" data-option="10"--}}
                        {{--data-second-option="0">--}}
                        {{--<div class="input-group">--}}
                        {{--<input type="text" class="form-control" name="validate-text" id="validate-text"--}}
                        {{--placeholder="Validate Text" required/>--}}
                        {{--<span class="input-group-addon danger">x</span>--}}
                        {{--</div>--}}
                        {{--<input style=" width: 50px;background: #4fb9e1;border: #4fb9e1;color: white;"--}}
                        {{--value=" Page" disabled/>--}}
                        <input style="width: 40px;height:40px;border-width: 2px; border-color:#4fb9e1 ;text-align: center;"
                               id="current" value="1"/>
                        <button type="button" class="btn btn-primary customColor "
                                data-method="setDragMode"
                                data-option="move" title="Move"
                                style="border-radius: 1px; margin-top: 1px">
                            <span class="docs-tooltip" data-toggle="tooltip">
                                <span> of</span>
                            </span>
                        </button>
                        <button type="button" class="btn btn-primary customColor "
                                data-method="setDragMode"
                                data-option="move" title="Move"
                                style="border-radius: 1px; margin-top: 1px">
                            <span class="docs-tooltip" data-toggle="tooltip">
                                <span id="total"> </span>
                            </span>
                        </button>
                        {{--<input id="total"--}}
                        {{--style="width: 50px;background-color: #4fb9e1;border-color: #4fb9e1;border-width: 0;color: #f4f3ef; "--}}
                        {{--disabled/>--}}
                        {{--</button>--}}


                        <button type="button" class="btn btn-primary customColor right-click" data-option="10"
                                data-second-option="0" title="Next Image" style="margin-top: 1px;border-radius: 0px">
                            <span class="docs-tooltip" data-toggle="tooltip" title="Next Image">
                                <span class="fa fa-arrow-right"></span>
                            </span>
                        </button>
                        <button type="button" class="btn btn-primary customColor forward-click"
                                data-method="setDragMode"
                                data-option="move" title="Move"
                                style="border-radius: 8px; margin-top: 1px">
                            <span class="docs-tooltip" data-toggle="tooltip" title="Last Image">
                                <span class="fa fa-fast-forward"> </span>
                            </span>
                        </button>
                        {{--<button type="button" class="btn btn-primary customColor forward-click"--}}
                        {{--data-method="setDragMode"--}}
                        {{--data-option="move"--}}
                        {{--title="Move">--}}
                        {{--<span class="docs-tooltip" data-toggle="tooltip" title="Last Image">--}}
                        {{--<span class="fa fa-fast-forward"></span>--}}
                        {{--</span>--}}
                        {{--</button>--}}
                        {{--</div>--}}

                        <button
                                style="background-color: transparent;
                                        border-width: 0;
                                        color: #4fb9e1;
                                        margin-top: 1px;
                                        font-size: 21px;
                                        margin-left: -5px;"
                                type="button" class="btn btn-primary customColor buttonRound newClass"
                                data-method="rotate"
                                data-option="-1"
                                title="Rotate Left">
                                <span class="docs-tooltip" data-toggle="tooltip" data-animation="false"
                                      title="Rotate Left">
                                  <span class="fa fa-undo-alt"></span>
                                </span>

                        </button>
                        <button
                                style="background-color: transparent;
                                        border-width: 0;
                                        color: #4fb9e1;
                                        margin-top: 1px;
                                        font-size: 21px;
                                        margin-left: -5px;"

                                type="button" class="btn btn-primary customColor buttonRound newClass"
                                data-method="rotate"
                                data-option="1"
                                title="Rotate Right">
            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false"
                  title="Rotate Right">
              <span class="fa fa-redo-alt"></span>
            </span>
                        </button>

                        <div class="btn-group btn-group-crop">
                            <button type="button" class="btn btn-success customColor" data-method="getCroppedCanvas"
                                    data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }">
            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false"
                  title="Clip and Save">
              Clip & Save
            </span>
                            </button>

                        </div>
                        <!-- Show the cropped image in modal -->
                        <div class="modal fade docs-cropped" id="getCroppedCanvasModal" aria-hidden="true"
                             aria-labelledby="getCroppedCanvasTitle" role="dialog" tabindex="-1">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="getCroppedCanvasTitle">Cropped</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body"></div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                        </button>
                                        <a class="btn btn-primary" id="download" href="javascript:void(0);"
                                           download="aaacropped.jpg">Download</a>
                                        <button type="button" class="btn btn-primary" id="export" export="cropped.jpg">
                                            Export
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal -->
                    </div>
                </div>
                <div style="width:1000px;margin:0px auto;">

                    <div id="thumbnail-slider"
                         {{--style="float:left;height: 515px;"--}}
                         {{--style="float:left;height: 515px;position: absolute;z-index: 99;top: 77px;"--}}
                         style="float:left;height: 515px;"
                    >
                        <div class="inner">
                            <?php $count = 0?>
                            <ul>
                                @foreach($images as $each)
                                    <li class="slider-click count
{{--{{$each->id==$selected?'active':''}}--}}
                                            " index="{{$count++}}" style="height: 212px;width: 140px">
                                        {{--<img src="{{asset('images/'.str_replace('-', '', $uploads->date).'/'.$uploads->publication.'/'.$each->image)}}"--}}
                                        {{--width="100%" height="90%" style="margin-top: -16px">--}}
                                        <a class="thumb"
                                           {{--                                           href="{{asset('images/'.str_replace('-', '', $upload->date).'/'.$upload->publication.'/'.$each->image)}}"></a>--}}

                                           href="{{$url.'images/'.str_replace('-', '', $upload->date).'/'.$upload->publication.'/'.$each->image}}"></a>
                                    </li>
                                @endforeach


                            </ul>
                        </div>
                    </div>
                    <div id="ninja-slider"
                         {{--style="float:right;"--}}
                         {{--style="float:right;width: 825px;position: absolute;z-index: 99;top: 78px;left: 212px;"--}}
                         style="float:right;width: 825px;"
                    >

                        <div class="slider-inner">
                            <div>
                                <!-- Wrapper for slides -->
                                <div>
                                    <div class="" style="width:1%; height:280px;margin-top: 206px; position: absolute ">
                                        <a href="javascript:;" class="left_arrow_pgright btn_prev left-click"
                                           style="text-decoration:none;"
                                           title="Previous" alt="Previous"> <span> <i class="fa fa-angle-left"
                                                                                      style="font-size: 50px"></i> </span>
                                        </a>
                                    </div>
                                    <div class=""
                                         style=" width:1%; height:280px;padding-left: 10px;margin-top: 206px;left: 790px; position: absolute">
                                        <a href="javascript:;" class="left_arrow_pgright btn_prev right-click"
                                           style="text-decoration:none;"
                                           title="Previous" alt="Next"> <span> <i class="fa fa-angle-right"
                                                                                  style="font-size: 50px"></i> </span>
                                        </a>
                                    </div>
                                    <div class="carousel-inner frame" style="float:left; width:94%; margin-left: 24px;">
                                        {{--<div class="item active img-container cropper-preview" id="cropper-preview">--}}
                                        {{--<img id="image-cropper"--}}
                                        {{--src="{{asset('images/'.str_replace('-', '', $upload->date).'/'.$upload->publication.'/'.$image->image)}}"--}}

                                        {{--style="margin-top: -16px">--}}
                                        {{--<img src="http://localhost:8000/images/20191209/1/3.JPG" alt="Los Angeles" style="width:100%;"--}}
                                        {{--class="">--}}
                                        {{--</div>--}}
                                        <div class="img-container">
                                            <div class="loader" id="loader"></div>

                                            <img id="image"
                                                 src="{{asset('images/'.str_replace('-', '', $upload->date).'/'.$upload->publication.'/'.$image->image)}}"
                                                 alt="Picture">
                                        </div>

                                    </div>

                                </div>
                                <!-- Left and right controls -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>


    <!-- Scripts -->
    <script src="{{asset('/crop/js/jquery-3.4.1.slim.min.js')}}" crossorigin="anonymous"></script>
    <script src="{{asset('/crop/js/bootstrap.bundle.min.js')}}"
            crossorigin="anonymous"></script>
    <script src="{{asset('/crop/js/google-analytics.js')}}" crossorigin="anonymous"></script>
    <script src="{{asset('/crop/js/cropper.js')}}" crossorigin="anonymous"></script>
    <script src="{{asset('/crop/js/jquery-cropper.js')}}"></script>
    {{--<script src="https://fengyuanchen.github.io/jquery-cropper/js/main.js"></script>--}}
    <script src="{{asset('/crop/js/main.js')}}"></script>
    <script src="{{asset('/crop/js/jspdf.min.js')}}"></script>
    <script>

        var $image = $('#image');
        $image.cropper({
            autoCropArea: 1,
            zoomOnWheel: false,
            viewMode: 4,

        });
        var list = [];
        var count = 0;

        $(document).ready(function () {
            $('#loader').show();
            var base_url = $('meta[name=url]').attr('content');
            var images = $('meta[name=images]').attr('content');
            var date = $('meta[name=date]').attr('content').replace(/-/g, '');
            var publication = $('meta[name=publication]').attr('content');
            var selected = $('meta[name=selected]').attr('content');
            count = selected;
            JSON.parse(images).forEach(function (each) {
                // var url = base_url + '/public/images/' + date + '/' + publication + '/' + each.image;
                var url = base_url + 'images/' + date + '/' + publication + '/' + each.image;
                list.push(url);
            });
            $('#total').text(' ' + list.length);
            jQuery($(".count")[count]).addClass('active');
            $image.cropper('replace', list[count]);
        });


        $('.left-click').on('click', function () {
            if (count != 0)
                count--;
            else
                count = list.length - 1;
            $('.active').removeClass('active');
            jQuery($(".count")[count]).addClass('active');
            $image.cropper('replace', list[count]);
            currentCount(count)
            $('#loader').show();
        });

        $('.right-click').on('click', function () {
            if (list.length - 1 != count)
                count++;
            else
                count = 0;

            $('.active').removeClass('active');
            jQuery($(".count")[count]).addClass('active');
            $image.cropper('replace', list[count])
            currentCount(count)
            $('#loader').show();

        });
        $('.slider-click').on('click', function () {
            count = $(this).attr('index');
            $image.cropper('replace', list[count])
            currentCount(count)
            $('#loader').show();

        });

        $('#export').on('click', function () {
            var publicationName = $('meta[name=publicationName]').attr('content');
            var date = $('meta[name=date]').attr('content').replace(/-/g, '');

            var cropped = $(this).prev().attr('href');
            var res = list[count].split("/");
            var name = res[res.length - 1];
            var doc = new jsPDF('p', 'pt', 'a4');
            var height = doc.internal.pageSize.height;
            var aspectwidth1 = (height - 50) * (9 / 16);
            doc.addImage(cropped, 'JPEG', 10, 50, aspectwidth1, (height - 50), '');
            doc.setTextColor(150);
            doc.text(50, doc.internal.pageSize.height - 30, publicationName + ' ' + date);
            doc.save(name + ".pdf");
        });
        $('#current').on('change', function () {
            if (list.length > $(this).val()) {
                count = $(this).val() - 1;
                $image.cropper('replace', list[count])
                $('.active').removeClass('active');
                jQuery($(".count")[count]).addClass('active');
            } else {
                $(this).val(list.length)
                count = list.length - 1;
                $image.cropper('replace', list[count])
                $('.active').removeClass('active');
                jQuery($(".count")[count]).addClass('active');
            }

        })

        $('.backward-click').on('click', function () {
            count = 0;
            $('.active').removeClass('active');
            jQuery($(".count")[count]).addClass('active');
            $image.cropper('replace', list[count]);
            currentCount(count)
            $('#loader').show();

        });

        $('.forward-click').on('click', function () {
            count = list.length - 1;
            $('.active').removeClass('active');
            jQuery($(".count")[count]).addClass('active');
            $image.cropper('replace', list[count]);
            currentCount(count)
            $('#loader').show();
        });

        window.addEventListener("load", function () {
            $('#loader').hide();
        })
        // window.on('event', function() {
        //     $('img').addClass('cropper-container').trigger('classChange');
        // });

        // in another js file, far, far away

        function currentCount(count) {
            $('#current').val(parseInt(count) + 1);
        }



    </script>

@endsection


