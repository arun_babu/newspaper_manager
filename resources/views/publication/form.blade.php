@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="places-buttons">

                        <div class="row">
                            <div class="col-lg-8 ml-auto mr-auto">
                                <div class="row">
                                    <div class="col-md-12">
                                        {{--@if(isset($publication))--}}
                                        {{--{{ Form::model($user, ['url' => ['store', $user->id], 'method' => 'patch']) }}--}}
                                        {{--@else--}}
                                        {{--{{ Form::open(['url' => 'store']) }}--}}
                                        {{--@endif--}}

                                        {{--{{ Form::text('fieldname1', Input::old('fieldname1')) }}--}}
                                        {{--{{ Form::text('fieldname2', Input::old('fieldname2')) }}--}}
                                        {{-- More fields... --}}
                                        {{--{{ Form::submit('Save', ['name' => 'submit']) }}--}}
                                        {{--{{ Form::close() }}--}}

                                        <form class="form" method="POST" action="{{ url('publication/store') }}">
                                            @csrf
                                            <input type="hidden" name="id"
                                                   value="{{isset($publication)?$publication->id:''}}">
                                            <div class="card-body ">
                                                <label>Publication</label>
                                                <div class="form-group">
                                                    <input name="name" class="form-control" required
                                                           value="{{isset($publication)?$publication->name:''}}">
                                                </div>
                                                <label>Description</label>
                                                <div class="form-group">
                                                    <input name="description" class="form-control" required
                                                           value="{{isset($publication)?$publication->description:''}}">
                                                </div>
                                            </div>
                                            <div class="card-footer ">
                                                <div class="col-md-12 text-center">
                                                    <button class="btn btn-primary" type="submit">
                                                        Save
                                                    </button>
                                                    <button class="btn btn-info" data-toggle="modal"
                                                            data-target="#noticeModal"
                                                            onclick="window.location='{{ url("publication") }}'">
                                                        Cancel
                                                    </button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection