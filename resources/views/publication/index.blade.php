@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card-body">
                <button class="btn btn-success"
                        {{--data-toggle="modal"--}}
                        {{--data-target="#noticeModal"--}}
                        onclick="window.location='{{ url("publication/form") }}'"
                >
                    Create
                </button>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"> Publications</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="text-primary">
                            <th class="text-center">
                                #
                            </th>
                            <th>
                                PUBLICATIONS
                            </th>
                            <th>
                                Description
                            </th>

                            <th class="text-right">
                                Actions
                            </th>
                            </thead>
                            <tbody>
                            <?php $count = 1;?>
                            @foreach($publications as $each)
                                <tr>
                                    <td class="text-center">
                                        {{$count++}}
                                    </td>
                                    <td>
                                        {{$each->name}}
                                    </td>
                                    <td>
                                        {{$each->description}}

                                    </td>
                                    <td class="text-right">
                                        <form method="DELETE" action="{{ url('publication/' . $each->id . '/delete') }}">
                                            <a href="{{ url('/publication/' . $each->id . '/edit') }}"
                                               class="btn btn-success btn-icon btn-sm "><i class="fa fa-edit"></i></a>
                                            <button type="submit" rel="tooltip" class="btn btn-danger btn-icon btn-sm ">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12 text-center">
                    <!-- Classic Modal -->
                    <div class="modal fade" id="noticeModal" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-notice">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        <i class="nc-icon nc-simple-remove"></i>
                                    </button>
                                    <h5 class="modal-title" id="myModalLabel">How Do You Become an
                                        Affiliate?</h5>
                                </div>
                                <div class="modal-body">
                                    <div class="instruction">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <strong>1. Register</strong>
                                                <p class="description">The first step is to create an account at
                                                    <a href="http://www.creative-tim.com/">Creative Tim</a>. You
                                                    can choose a social network or go for the classic version,
                                                    whatever works best for you.</p>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="picture">
                                                    <img src="../../assets/img/bg/daniel-olahs.jpg"
                                                         alt="Thumbnail Image" class="rounded img-raised">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="instruction">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <strong>2. Apply</strong>
                                                <p class="description">The first step is to create an account at
                                                    <a href="http://www.creative-tim.com/">Creative Tim</a>. You
                                                    can choose a social network or go for the classic version,
                                                    whatever works best for you.</p>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="picture">
                                                    <img src="../../assets/img/bg/david-marcu.jpg"
                                                         alt="Thumbnail Image" class="rounded img-raised">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>If you have more questions, don't hesitate to contact us or send us a
                                        tweet @creativetim. We're here to help!</p>
                                </div>
                                <div class="modal-footer justify-content-center">
                                    <button type="button" class="btn btn-info btn-round" data-dismiss="modal">
                                        Sounds good!
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end notice modal -->

                </div>
            </div>

        </div>


    </div>
@endsection