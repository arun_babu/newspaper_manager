<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent" >
    <div class="container-fluid" >
        <div class="container-fluid"  style="text-align: center">

                <h2 style="margin-bottom: 0px"><b>DIGITAL ARCHIVING SYSTEM</b></h2>

            {{--<div class="navbar-minimize">--}}
                {{--<button id="minimizeSidebar" class="btn btn-icon btn-round">--}}
                    {{--<i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>--}}
                    {{--<i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>--}}
                {{--</button>--}}
            {{--</div>--}}
            {{--<div class="navbar-toggle">--}}
                {{--<button type="button" class="navbar-toggler">--}}
                    {{--<span class="navbar-toggler-bar bar1"></span>--}}
                    {{--<span class="navbar-toggler-bar bar2"></span>--}}
                    {{--<span class="navbar-toggler-bar bar3"></span>--}}
                {{--</button>--}}
            {{--</div>--}}
            {{--<a class="navbar-brand" href="#pablo">Digital Archiving System </a>--}}
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
                aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <ul class="navbar-nav">
                {{--@if (auth()->user()->is_admin == 1)--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link btn-magnify" href="{{ url('my-profile') }}">--}}
                            {{--<i class="nc-icon nc-circle-10" style="top:2px"></i>--}}
                            {{--<span>Profile</span>--}}

                            {{--<p>--}}
                                {{--<span class="d-lg-none d-md-block">Stats</span>--}}
                            {{--</p>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                {{--@endif--}}
                {{--<li class="nav-item btn-rotate dropdown">--}}
                {{--<a class="nav-link dropdown-toggle" href="http://example.com/" id="navbarDropdownMenuLink"--}}
                {{--data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                {{--<i class="nc-icon nc-bell-55"></i>--}}
                {{--<p>--}}
                {{--<span class="d-lg-none d-md-block">Some Actions</span>--}}
                {{--</p>--}}
                {{--</a>--}}
                {{--<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">--}}
                {{--<a class="dropdown-item" href="#">Action</a>--}}
                {{--<a class="dropdown-item" href="#">Another action</a>--}}
                {{--<a class="dropdown-item" href="#">Something else here</a>--}}
                {{--</div>--}}
                {{--</li>--}}
                <li class="nav-item" style="width: 115px;">
                    <a class="nav-link " href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="nc-icon nc-button-power" style="top:2px"></i>

                        <span>LogOut</span>
                        <p>
                            <span class="d-lg-none d-md-block">Account</span>
                        </p>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>

        @if (session('flash_message'))
            {{--@if (session('success'))--}}
            <div class="alert alert-success " id="successMessage"
                 style="display: inline-block; margin: 0px auto; position: fixed;  z-index: 1031; top: 20px; right: 20px;">
                <button type="button" aria-hidden="true" class="close">×</button>
                <span> User Created  Successfully</span>
            </div>
            {{--<div class="alert alert-success alert-block">--}}
            {{--<button type="button" class="close" data-dismiss="alert">×</button>--}}
            {{--<strong>{{ $message }}</strong>--}}
            {{--</div>--}}
        @endif
    </div>
</nav>
