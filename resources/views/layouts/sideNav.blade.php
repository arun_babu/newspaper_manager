
<ul class="nav">
    @if (auth()->user()->is_admin == 1)
        <li class="{{(request()->segment(1)=='home')?'active':''}}">
            <a href="{{ route('home') }}">
                <i class="nc-icon nc-bank"></i>
                <p>Dashboard
                </p>
            </a>
        </li>
        <li class="{{(request()->segment(1)=='upload')?'active':''}}">
            <a href="{{ url('upload') }}">
                <i class="nc-icon nc-cloud-upload-94"></i>
                <p>Upload</p>
            </a>
        </li>
        <li class="{{(request()->segment(1)=='search')?'active':''}}">
            <a href="{{ url('search') }}">
                <i class="nc-icon nc-zoom-split"></i>
                <p>Search</p>
            </a>
        </li>
        <li class="{{(request()->segment(1)=='publication')?'active':''}}">
            <a href="{{ url('publication') }}">
                <i class="nc-icon nc-paper"></i>
                <p>Daily</p>
            </a>
        </li>
        <li class="{{(request()->segment(1)=='profile')?'active':''}}">
            <a href="{{ url('profile') }}">
                <i class="nc-icon nc-bookmark-2"></i>
                <p>Registration</p>
            </a>
        </li>
        <li class="{{(request()->segment(1)=='users')?'active':''}}">
            <a href="{{ url('users') }}">
                <i class="nc-icon nc-single-02"></i>
                <p>Users</p>
            </a>
        </li>
        <li class="{{(request()->segment(1)=='my-profile')?'active':''}}">
            <a href="{{ url('my-profile') }}">
                <i class="nc-icon nc-book-bookmark"></i>
                <p>Profile</p>
            </a>
        </li>
    @else
        <li class="{{(request()->segment(1)=='home')?'active':''}}">
            <a href="{{ route('home') }}">
                <i class="nc-icon nc-bank"></i>
                <p>Dashboard
                </p>
            </a>
        </li>

        <li class="{{(request()->segment(1)=='search')?'active':''}}">
            <a href="{{ url('search') }}">
                <i class="nc-icon nc-zoom-split"></i>
                <p>Search</p>
            </a>
        </li>

    @endif

</ul>