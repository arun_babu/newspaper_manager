<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Upload extends Model
{
    //
    use SoftDeletes;

    public function getPublicationById()
    {
        return $this->belongsTo('App\Publication', 'publication','id');
    }

}
