<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Twilio\Rest\Client;
use Authy\AuthyApi;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $authy;
    protected $sid;
    protected $authToken;
    protected $twilioFrom;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        // Initialize the Authy API and the Twilio Client
        $this->authy = new AuthyApi(config('app.twilio')['AUTHY_API_KEY']);
        // Twilio credentials
        $this->sid = config('app.twilio')['TWILIO_ACCOUNT_SID'];
        $this->authToken = config('app.twilio')['TWILIO_AUTH_TOKEN'];
        $this->twilioFrom = config('app.twilio')['TWILIO_PHONE'];
    }

    public function loginWithOtp(Request $request)
    {
//        $request->phone;
//        $user = \App\User::where('mobile_no', 9)->first();
        $user = User::where('id', 9)->first();
        if ($user) {
            Auth::login($user);
            return redirect()->route('home');
        } else {
            return redirect()->back();
        }
    }

    public function verifyPhone(Request $request)
    {

//        $user = User::where('mobile', $request->phone)->first();
      
        $user = User::where('mobile_no', $request->phone)->first();

//        if ($user) {
        if ($user) {
            // Validate form input
//        $this->validate($request, [
//            'country_code' => 'required|string|max:3',
//            'phone' => 'required|string|max:10',
//            'via' => 'required|string'
//        ]);
//
//        //Call the “phoneVerification” method from the Authy API and pass the phone number, country code and verification channel(whether sms or call) as parameters to this method.
            $response = $this->authy->phoneVerificationStart($request->phone, +91, 'sms');
//

            if ($response->ok()) {
                $status = true;
            } else {
                $status = false;
            }
            return response()->json(['status' => $status]);
        } else {
            return '';
        }
    }


    public function verifyCode(Request $request)
    {
//         Call the method responsible for checking the verification code sent.
        $response = $this->authy->phoneVerificationCheck($request->phone, +91, $request->code);
        if ($response->ok()) {
            $user = User::where('mobile_no', $request->phone)->first();
            Auth::login($user);
            $status = true;
        } else {
            $status = false;
        }
        return response()->json(['status' => $status]);
    }
}
