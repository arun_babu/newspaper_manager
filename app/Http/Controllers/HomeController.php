<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

//        return view('newspaper.upload', compact('message'));

        return view('home');
//        return redirect('home')->with('success', 'You have done successfully');

//        return back()->with('success','Item created successfully!');

    }
}
