<?php
/**
 * Created by PhpStorm.
 * User: cyfous
 * Date: 19/11/19
 * Time: 10:23 AM
 */

namespace App\Http\Controllers;

use  App\Publication as Publication;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PublicationController extends Controller
{

    /**
     *
     * @return View
     */
    public function index()
    {
        $publications = Publication::where('deleted_at', NULL)
            ->get();
        return view('publication.index', compact('publications'));
    }

    /**
     *
     * @return View
     */
    public function form()
    {
        return view('publication.form');
    }

    /**
     *
     * @param  Request $request
     * @return Json
     */
    public function upload(Request $request)
    {
        $uploadedFile = $request->file('file');
        $filename = time() . $uploadedFile->getClientOriginalName();

        Storage::disk('local')->putFileAs(
            'files/' . $filename,
            $uploadedFile,
            $filename
        );

        $upload = new Upload;
        $upload->filename = $filename;

        $upload->user()->associate(auth()->user());

        $upload->save();

        return response()->json([
            'id' => $upload->id
        ]);
    }

    /**
     *
     * @param  Request $request
     * @return url
     */
    public function store(Request $request)
    {

        if ($request->id) {
            $publication = Publication::find($request->id);
        } else {
            $publication = new Publication;

        }
        $publication->name = $request->name;
        $publication->description = $request->description;
        $publication->save();

        return redirect('publication');
    }

    /**
     *
     * @param  $id
     * @return View
     */
    public function edit($id)
    {
        $publication = Publication::find($id);
        return view('publication.form', compact('publication'));

    }

    /**
     *
     * @param  $id
     * @return redirect
     */
    public function delete($id)
    {
        $id = Publication::find($id);
        $id->delete();
        return redirect('publication');
    }

    /**
     *
     * @return Json
     */
    public function getPublication()
    {
        $publications = Publication::where('deleted_at', NULL)
            ->get();
        return response()->json($publications);
    }

}