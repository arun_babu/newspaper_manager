<?php
/**
 * Created by PhpStorm.
 * User: cyfous
 * Date: 18/11/19
 * Time: 6:26 PM
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


use App\User;
use Illuminate\Http\Request;
use Illuminate\View\View;


class ProfileController
{


    /**
     *
     * @return View
     */
    public function index()
    {
        $users = User::all();
        return view('profile.index', compact('users'));
    }

    /**
     *
     * @return View
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('profile.edit', compact('user'));
    }

    /**
     *
     * @return View
     */
    public function form()
    {
        return view('profile.form');

    }

    public function store(Request $request)
    {
//        $request->validate([
////            'email' => 'required|email|unique:users',
////            'mobile_no' => 'required|mobile_no|unique:users',
//            'mobile_no' => 'required',
//        ]);
        $validator = $request->validate([
            'email' => 'required|email|unique:users,email,' . $request->id,
            'mobile_no' => 'required|unique:users,mobile_no,' . $request->id,
        ]);

        $user = User::find($request->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile_no = $request->mobile_no;
        $user->save();
        return view('profile.edit', compact('user'));
    }

    public function profile()
    {
        $user = User::find(Auth::user()->id);

        return view('profile.edit', compact('user'));
    }

}
