<?php
/**
 * Created by PhpStorm.
 * User: cyfous
 * Date: 18/11/19
 * Time: 6:17 PM
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use  App\Publication as Publication;
use App\Upload as Upload;
use App\Image as Image;
use Carbon\Carbon as Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\View\View;
use Psy\Util\Json;


class UploadController extends Controller
{

    /**
     *
     * @return View
     */
    public function upload()
    {
        $publications = Publication::where('deleted_at', NULL)
            ->get();
        return view('newspaper.upload', compact('publications'));
    }

    /**
     *
     * @return View
     */
    public function test()
    {
        return view('newspaper.test');
    }

    /**
     *
     * @return View
     */
    public function testing()
    {
        return view('newspaper.testing');
    }

    /**
     * Save a image on storage location form incoming request.
     *
     * @param  Request $request
     * @return Json
     */

    public function save(Request $request)
    {


        $RequestDate = Carbon::parse($request->date)->format('Y-m-d');
        if ($request->id) {
            Upload::where('id', $request->id)
                ->update(['publication' => $request->publication, 'date' => Carbon::parse($RequestDate)->format('Y-m-d')]);
            Image::where('upload_id', $request->id)->delete();


            if ($request->hasFile('file')) {
                $name = str_replace(' ', '_', $request->file->getClientOriginalName());
                $filePath = 'images/' . str_replace('-', '', $RequestDate) . '/' . $request->publication . '/' . $name;
                Storage::disk('s3')->put($filePath, file_get_contents($request->file));
                $image = new Image;
                $image->upload_id = $request->id;
                $image->image = str_replace(' ', '_', $name);
                $image->save();
            }

        } else {

            $isAvilabile = Upload::where('publication', $request->publication)->where('date', Carbon::parse($RequestDate)->format('Y-m-d'))->first();
//            if ($isAvilabile != null) {
//                Upload::where('publication', $request->publication)
//                    ->where('date', Carbon::parse($request->date)->format('Y-m-d'))
//                    ->delete();
//            }
            if (!$isAvilabile) {
                $upload = new Upload;
                $upload->publication = $request->publication;
                $upload->date = Carbon::parse($RequestDate)->format('Y-m-d');
                $upload->save();
                $uploadid = $upload->id;
            } else {
                $uploadid = $isAvilabile->id;
            }

            if ($request->hasFile('file')) {
                $name = str_replace(' ', '_', $request->file->getClientOriginalName());
                $filePath = 'images/' . str_replace('-', '', $RequestDate) . '/' . $request->publication . '/' . $name;
                Storage::disk('s3')->put($filePath, file_get_contents($request->file));
                $image = new Image;
                $image->upload_id = $uploadid;
                $image->image = $name;
                $image->save();
            }
        }
        $status = true;
        return response()->json([
            'status' => $status,
        ]);

    }

    /**
     *
     * @param  Request $request
     * @return Json
     */
    public function isExist(Request $request)
    {
        $isAvilabile = Upload::where('publication', $request->publication)
            ->where('date', Carbon::parse($request->datepicker)->format('Y-m-d'))
            ->first();
        $status = $isAvilabile != null;
        return response()->json([
            'status' => $status,
        ]);
    }

    /**
     *
     * @param  $id
     * @return View
     */
    public function lists($id)
    {
//        https://news-paper.s3.ap-south-1.amazonaws.com/images/20191218/1/2.JPG
        $url = 'https://' . env('AWS_BUCKET') . '.s3.' . env('AWS_DEFAULT_REGION') . '.amazonaws.com/';
//        $url = 'https://s3.' . env('AWS_DEFAULT_REGION') . '.amazonaws.com/' . env('AWS_BUCKET') . '/';


        $uploads = Upload::where('deleted_at', NULL)
            ->where('id', $id)
            ->first();

        $images = Image::where('deleted_at', NULL)
            ->where('upload_id', $id)
            ->orderByRaw('LENGTH(image)', 'ASC')
            ->orderBy('image','ASC')
            ->get();

        return view('newspaper.list', compact('images', 'uploads', 'url'));
    }

    /**
     *
     * @param  Request $request
     * @return View
     */
    public function search(Request $request)
    {
        $publications = Publication::where('deleted_at', NULL)
            ->get();
        if ($request->all() == null) {
            $start = date('Y-m-d', strtotime('-29 days', strtotime(date("m/d/Y"))));
            $end = date("m/d/Y");
            $uploads = Upload::where('deleted_at', NULL)
                ->whereBetween('date', [Carbon::parse($start)->format('Y-m-d'), Carbon::parse($end)->format('Y-m-d')])
                ->get();
        } else {
            if ($request->publication == 'null') {
                $uploads = Upload::where('deleted_at', NULL)
                    ->whereBetween('date', [Carbon::parse($request->start)->format('Y-m-d'), Carbon::parse($request->end)->format('Y-m-d')])
                    ->get();
            } else {
                $uploads = Upload::where('deleted_at', NULL)
                    ->whereBetween('date', [Carbon::parse($request->start)->format('Y-m-d'), Carbon::parse($request->end)->format('Y-m-d')])
                    ->where('publication', $request->publication)
                    ->get();
            }
        }
        return view('upload.search', compact('publications', 'uploads'));
    }

    /**
     *
     * @param  $id
     * @return View
     */
    public function crop($id)
    {
        $url = 'https://' . env('AWS_BUCKET') . '.s3.' . env('AWS_DEFAULT_REGION') . '.amazonaws.com/';

        $image = Image::where('deleted_at', NULL)
            ->where('id', '=', $id)
            ->first();
        $upload = Upload::where('deleted_at', NULL)
            ->where('id', $image->upload_id)
            ->first();

        $images = Image::where('deleted_at', NULL)
            ->where('upload_id', '=', $image->upload_id)
            ->orderByRaw('LENGTH(image)', 'ASC')
            ->orderBy('image','ASC')
            ->get();
        $selected = 0;
        foreach ($images as $key => $value)
            if ($value->id == $id)
                $selected = $key;
        return view('upload.crop', compact('images', 'image', 'upload', 'selected', 'url'));
    }


    /**
     *
     * @param  Request $request
     * @return Json
     */
    public function deleteExist(Request $request)
    {
        $isAvilabile = Upload::where('publication', $request->publication)->where('date', Carbon::parse($request->datepicker)->format('Y-m-d'))->delete();

        return response()->json([
            'status' => $isAvilabile,
        ]);
    }
}